import os
import pyphysio as ph

ecg_dir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl/ecg'
ibi_dir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl/ibi'
trg_dir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl/trg'

#use only subjects with a trg
subjects = os.listdir(trg_dir)

ibi_detector = ph.BeatFromECG()

#%%
#to distribute the workload, use
#for sub in subjects[IDX_START:IDX_STOP]:
for sub in subjects:
    print(sub)
    ecg = ph.from_pickle(f'{ecg_dir}/{sub}')
    ibi = ibi_detector(ecg)
    
    ibi_ok = ph.Annotate(ecg, ibi).ibi_ok
    
    ibi_ok.to_pickle(f'{ibi_dir}/{sub}')
