#average for each subject by stimulus
import numpy as np
import os
import pandas as pd

DATADIR = '/home/bizzego/UniTn/data/GxE_Cataldo/hrv'

subjects = os.listdir(DATADIR)

metadata = pd.read_csv('/home/bizzego/UniTn/data/GxE_Cataldo/original/SNG_PBI_ASQ_ECR_DNA_selected.csv', index_col=0)

#%%
data_all = []
for sub in subjects:
    data_sub = pd.read_csv(f'{DATADIR}/{sub}', index_col=0)
    
    #normalize RRmean
    data_sub['RRmean_normBaseline'] = data_sub['RRmean'] - data_sub['RRmean'][0]
    data_sub['RRmean_normStimuli'] = 0
    data_sub['RRmean_normStimuli'].iloc[1:] = data_sub['RRmean'][1:].values - data_sub['RRmean'][:-1].values
    
    #only select stimuli and
    #compute average by stimulus
    data_aggr = data_sub.query('label>0').groupby('label').mean()
    data_aggr['label'] = data_aggr.index
    
    #drop cols
    data_aggr.drop(columns=['begin','end'], inplace=True)
    
    #add metadata
    data_aggr['subject'] = sub.split('.')[0]
    data_aggr = pd.merge(data_aggr, metadata, left_on='subject', right_index=True)
    
    data_all.append(data_aggr)

#%%
data_all = pd.concat(data_all)
data_all.to_csv('/home/bizzego/UniTn/data/GxE_Cataldo/dataset.csv')
