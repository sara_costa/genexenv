import pandas as pd
import os

data = pd.read_csv('/home/bizzego/UniTn/data/GxE_Cataldo/dataset.csv', index_col=0)
metadata = pd.read_csv('/home/bizzego/UniTn/data/GxE_Cataldo/original/SNG_PBI_ASQ_ECR_DNA.csv', index_col=0)

#%%
subjects = data['subject'].unique()
metadata_selected = metadata[metadata.index.isin(subjects)]
metadata_selected.to_csv('/home/bizzego/UniTn/data/GxE_Cataldo/original/SNG_PBI_ASQ_ECR_DNA_selected.csv')
