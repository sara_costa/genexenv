import os
import pandas as pd
import numpy as np
import pyphysio as ph

datadir = '/home/bizzego/UniTn/data/GxE_Cataldo/original/ECG_match_NIRS_Log_onset'
ecgdir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl/ecg'
outdir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl/trg'
outdir_df = '/home/bizzego/UniTn/data/GxE_Cataldo/timestamps'
subjects = os.listdir(datadir)

stim_dict = {'Instructions': -1,
             'interstimuli': 0,
             'cat' : 1,
             '3month': 2,
             '12month': 3,
             'woman' : 4}

subjects_with_ecg = [x.split('.')[0] for x in os.listdir(ecgdir)]

#%%
for sub in subjects:
    subname = sub.split('.')[0]
    
    if sub.endswith('.tln') and subname in subjects_with_ecg:
        
        data = pd.read_csv(f'{datadir}/{sub}', sep='\t', skiprows=8, names = ['time', 'nn', 'marker', 'type', 'stimulus'])
        
        nsamples = int((data['time'].values[-1]+10) * 100)
        trg = np.zeros(nsamples)
        
        t_st = 0
        for i_row in np.arange(len(data)-1):
            curr_row = data.iloc[i_row, :]
            portion_type = curr_row['stimulus'].split('_')[0]
            portion_value = stim_dict[portion_type]
            t_sp =  data.iloc[i_row+1, :]['time']
            idx_st = int(t_st*100)
            idx_sp = int(t_sp*100)
            trg[idx_st:idx_sp] = portion_value
            t_st = t_sp
        
        curr_row = data.iloc[-1, :]
        portion_type = curr_row['stimulus'].split('_')[0]
        portion_value = stim_dict[portion_type]
        idx_st = int(t_st*100)
        trg[idx_st:] = portion_value

        trg = ph.EvenlySignal(trg, 100)
        subname = sub.split('.')[0]
        trg.to_pickle(f'{outdir}/{subname}.pkl')
        
        data.to_csv(f'{outdir_df}/{subname}.csv')
        
    else:
        if sub.endswith('.tln'):
            print(sub)
            