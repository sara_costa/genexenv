import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels.stats.anova import AnovaRM
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import pingouin as pg
import warnings

warnings.filterwarnings('ignore')
datafile = '/home/bizzego/UniTn/data/GxE_Cataldo/dataset.csv'

TARGET = 'RRmean_normStimuli'

data = pd.read_csv(datafile, index_col=0)
data = data.query('label >= 1')
data.dropna(axis=0, inplace=True)

#%% add vulnerability columns
data['vuln_76'] = 0
data.loc[(data['rs53576'] == 'GG'), 'vuln_76'] = 1

data['vuln_98'] = 0 #!!! CORRECT?
data.loc[(data['rs2254298'] == 'AA'), 'vuln_98'] = 1 #!!! CORRECT?

#%% FIRST ANALISIS
#average response by subject
# Gene X Env model for each type of stimulus
# hrv ~ C(Gene) + env + C(Gene)*env

# data_averages = data.groupby(['subject']).mean().reset_index()
data_averages = data.groupby(['subject', 'label']).mean().reset_index()

genes = ['vuln_76', 'vuln_98']
envs = ['PBI_M_care', 'PBI_M_overp', 'PBI_F_care', 'PBI_F_overp']

for gene_col in genes:
    # data_nonvuln = data_averages.query(f'{gene_col} == 0 ')
    # data_vuln = data_averages.query(f'{gene_col} == 1 ')
    
    for env_col in envs:
        
        print(f'{gene_col} x {env_col}\n')
        aovrm2way = pg.mixed_anova(data = data, dv=TARGET, 
                                   within='label',
                                   between=[gene_col, env_col],
                                   subject = 'subject')
        # res2way = aovrm2way.fit()

        print(aovrm2way[['Source', 'F', 'p-unc', 'p-GG-corr', 'np2']])  
        print('\n\n')
        
#%% Significant results>>>
gene_col = 'vuln_76'
env_col = 'PBI_M_overp'
f, axes = plt.subplots(1,2, sharex=True, sharey=True)

f.suptitle(f'{gene_col} x {env_col}')

plt.sca(axes[0])
plt.plot(data_vuln[env_col], data_vuln[TARGET], 'or', alpha=0.15)
plt.plot(data_nonvuln[env_col], data_nonvuln[TARGET], 'ob')
plt.grid(True)
plt.xlabel(env_col)
plt.ylabel(TARGET)

plt.sca(axes[1])
plt.plot(data_nonvuln[env_col], data_nonvuln[TARGET], 'ob', alpha=0.15)
plt.plot(data_vuln[env_col], data_vuln[TARGET], 'or')
plt.grid(True)
plt.xlabel(env_col)
plt.ylabel(TARGET)

#%% SECOND ANALISIS
# Gene X Env model for each type of stimulus
# hrv ~ C(Gene) + env + C(Gene)*env

genes = ['vuln_76']
envs = ['PBI_M_care', 'PBI_M_overp', 'PBI_F_care', 'PBI_F_overp']

for label in [1,2,3,4]:
    print(label)
    data_label = data.query('label == @label')
    data_label = data_label.groupby(['subject']).mean().reset_index()
    for gene_col in genes:
        data_nonvuln = data_label.query(f'{gene_col} == 0 ')
        data_vuln = data_label.query(f'{gene_col} == 1 ')
            
        for env_col in envs:
            formula = f'{TARGET} ~ C({gene_col}) + {env_col} + C({gene_col})*{env_col}'
            model = smf.ols(formula=formula, data=data_label).fit()
            print('\n')
            print(f'{gene_col} x {env_col}')
            print(model.summary())
            
            # f, axes = plt.subplots(1,1)
            
            # f.suptitle(f'{gene_col} x {env_col}')
            # plt.sca(axes)
            # plt.plot(data_nonvuln[env_col], data_nonvuln[TARGET], 'ob', alpha=0.5)
            # plt.plot(data_vuln[env_col], data_vuln[TARGET], 'or', alpha=0.5)
            # plt.grid(True)
            # plt.xlabel(env_col)
            # plt.ylabel(TARGET)
            
            # plt.legend(['Non Vulnerable', 'Vulnerable'])

#%% Significant results>>>
label = 2
data_label = data.query('label == @label')
data_label = data_label.groupby(['subject']).mean().reset_index()

gene_col = 'vuln_76'
env_col = 'PBI_M_overp'

data_nonvuln = data_label.query(f'{gene_col} == 0 ')
data_vuln = data_label.query(f'{gene_col} == 1 ')

formula = f'{TARGET} ~ C({gene_col}) + {env_col} + C({gene_col})*{env_col}'
model = smf.ols(formula=formula, data=data_label).fit()
print('\n')
print(f'{gene_col} x {env_col}')
print(model.summary())

f, axes = plt.subplots(1,2, sharex=True, sharey=True)

f.suptitle(f'{gene_col} x {env_col}')

plt.sca(axes[0])
plt.plot(data_vuln[env_col], data_vuln[TARGET], 'or', alpha=0.15)
plt.plot(data_nonvuln[env_col], data_nonvuln[TARGET], 'ob')
plt.grid(True)
plt.xlabel(env_col)
plt.ylabel(TARGET)

plt.sca(axes[1])
plt.plot(data_nonvuln[env_col], data_nonvuln[TARGET], 'ob', alpha=0.15)
plt.plot(data_vuln[env_col], data_vuln[TARGET], 'or')
plt.grid(True)
plt.xlabel(env_col)
plt.ylabel(TARGET)

