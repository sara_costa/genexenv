import os
import pandas as pd
import pyphysio as ph
import matplotlib.pyplot as plt

ibi_dir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl/ibi'
trg_dir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl/trg'
hrv_dir = '/home/bizzego/UniTn/data/GxE_Cataldo/hrv'
#use only subjects with ibi
subjects = os.listdir(ibi_dir)

#%%
for sub in subjects:

    trg = ph.from_pickle(f'{trg_dir}/{sub}')
    ibi = ph.from_pickle(f'{ibi_dir}/{sub}')
    
    #% filter ibi
    id_bad_ibi = ph.BeatOutliers(sensitivity = 0.75)(ibi)
    
    if len(id_bad_ibi)>0:
        plt.figure()
        ibi.plot('r')
        
        ibi = ph.FixIBI(id_bad_ibi)(ibi)
        
        ibi.plot()

    #%
    segmenter = ph.LabelSegments(trg)
    hrv_indicators = [ph.Mean(name='RRmean')]
    
    results, columns = ph.fmap(segmenter, hrv_indicators, ibi)
    results = pd.DataFrame(results, columns = columns)
    
    subname = sub.split('.')[0]
    results.to_csv(f'{hrv_dir}/{subname}.csv')
