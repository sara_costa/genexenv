import os
import pandas as pd
import pyphysio as ph

datadir = '/home/bizzego/UniTn/data/GxE_Cataldo/original/Cataldo_G_E_ECG'
outdir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl/ecg'
subjects = os.listdir(datadir)

#%%
for sub in subjects:
    if sub.endswith('.txt'):
        
        data = pd.read_csv(f'{datadir}/{sub}', sep='\t', skiprows=3, names = ["nSeq", "I1", "I2", "O1", "O2", "A2"])
        ecg = ph.EvenlySignal(data['O2'].values, 1000)
        subname = sub.split('_')[0]
        ecg.to_pickle(f'{outdir}/{subname}.pkl')
