import pyphysio as ph
import matplotlib.pyplot as plt
import os
import numpy as np

datadir = '/home/bizzego/UniTn/data/GxE_Cataldo/pkl'

#%%
subjects = ['DIN27CAI', 'HAO21TAN', 'ISA16HO',
            'LQX12XU', 'MAV24TAN', 'WEN23NEO',
            'YIH24TAN']

#%%
for sub in subjects:

    ecg = ph.from_pickle(f'{datadir}/ecg/{sub}.pkl')
    ibi = ph.from_pickle(f'{datadir}/ibi/{sub}.pkl')
    
    
    figures, axes = plt.subplots(2,1, sharex=True)
    plt.sca(axes[0])
    ecg.plot()
    plt.vlines(ibi.get_times(), ecg.min(), ecg.max(), 'gray', linewidth=0.5)
    
    plt.sca(axes[1])
    ibi.plot()
    plt.vlines(ibi.get_times(), ibi.min(), ibi.max(), 'gray', linewidth=0.5)


